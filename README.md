# Nixos on gitlab ci!

## Are you going to upstream this?
No

## Installation

Add this to your nixos configuration:

```
imports = [ ./gitlab-runner.nix];


services.nix-gitlab-runner.enable = true;
services.nix-gitlab-runner.registrationConfigFile = "/var/lib/secrets/mysecret"
services.nix-gitlab-runner.runnerName = "my server"; # optional


```

The registration config file contains the secretes used to connect to
your gitlab instance. You should provision it manually, or
using a tool like `nixops` or `vault`.

The content of the file should be as follows:

```
CI_SERVER_URL=<CI server URL>
REGISTRATION_TOKEN=<registration secret>

```

And you're all set! You can now build Nix projects using
all nix commands. Check
out the `.gitlab-ci.yml` for an example

The module will
* Automatically register on startup
* Automatically deregister at shutdown
* Gracefully finish all builds


## How does it work

It just uses the shell executor 


## Pros
* Caching between builds
* Fast rebuilds
* Sharing between builds, _even between different gitlab projects_.


## Cons
You have to clean up after yourself. Docker will not save you.





